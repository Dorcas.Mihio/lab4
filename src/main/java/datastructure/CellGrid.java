package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    int rows;
    int columns;
    CellState initialState;
    CellState[][]Grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows=rows;
        this.columns=columns;
        this.initialState=initialState;
        Grid= new CellState[rows][columns];
	}


    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
       if (!(numRows()>row && row>=0))
           throw new IllegalArgumentException();
       if (!(numColumns()>column && column>=0))
            throw new IllegalArgumentException();
       Grid[row][column]=element;
    }

    @Override
    public CellState get(int row, int column) {


        // TODO Auto-generated method stub

        return Grid[row][column] ;
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid gridCopy= new CellGrid(this.rows,this.columns,CellState.DEAD);
        for(int i=0; i< this.rows; i++){
            for (int j=0; j< this.columns;j++){
                CellState forCellstate= this.get(i,j);
                gridCopy.set(i,j,forCellstate);
            }
        }
        return gridCopy;
    }
    
}
